
/// <reference path="../typings/index.d.ts" />




class Default {



    public scene: THREE.Scene = new THREE.Scene();
    public camera: THREE.Camera;
    public controls: THREE.OrbitControls;
    public renderer: THREE.WebGLRenderer;


    public constructor() {

        this.camera = this.createCamera();
        this.controls = this.createControls();
        this.renderer = this.createRender();
        this.setupRenderer();
        this.createCube();

        this.renderer.render(this.scene, this.camera);
    

    }

    public createCamera(): THREE.Camera {

        var camera: THREE.Camera = new THREE.PerspectiveCamera(
            75, //fov
            window.innerWidth / window.innerHeight, //resolution
            0.1,
            1000);

        camera.position.x = 1;
        camera.position.y = 1;
        camera.position.z = 5;
        return camera
    }

    public createControls(): THREE.OrbitControls {

        var controls = new THREE.OrbitControls(this.camera);
        (<any>controls).noKeys = false;

        return controls;
    }

    private createRender() {
        return new THREE.WebGLRenderer({
            antialias: true,
            alpha: true
        });
    }

    public setupRenderer() {


        this.renderer.setSize(window.innerWidth, window.innerHeight);
        document.body.appendChild(this.renderer.domElement);


    }


    public createCube() {

        var geometryCube: THREE.BoxGeometry = new THREE.BoxGeometry(1, 1, 1);
        var materialNormal: THREE.MeshNormalMaterial = new THREE.MeshNormalMaterial();
        var cube: THREE.Mesh = new THREE.Mesh(geometryCube, )
        this.scene.add(cube);

    }


    /*
    var geometryCube = new THREE.BoxGeometry(1, 1, 1);
    var geometrySphere = new THREE.SphereGeometry(1, 100, 100);
    
    var materialNormal = new THREE.MeshNormalMaterial();
    var materialPhong = new THREE.MeshPhongMaterial({
        color: 0x00ee9a,
        shininess: 10000
    });
    var materialLambert = new THREE.MeshLambertMaterial(0xaaaaaa);
    
    
    var cube = new THREE.Mesh(geometryCube, materialNormal);
    scene.add(cube);
    
    var mesh1 = new THREE.Mesh(geometrySphere, materialPhong);
    mesh1.position.x = -2;
    scene.add(mesh1);
    
    var mesh2 = new THREE.Mesh(geometrySphere.clone(), materialLambert);
    mesh2.position.x = 2;
    scene.add(mesh2)
    
    var light = new THREE.DirectionalLight(0xdddddd, 1);
    light.position.set(0, 0, 1);
    scene.add(light);
    
    
    
    camera.position.x = 1;
    camera.position.y = 1;
    camera.position.z = 5;
    
    var clock = new THREE.Clock();
    var render = function () {
        requestAnimationFrame(render);
        var delta = clock.getDelta();
        cube.rotation.x += delta;
        cube.rotation.y += delta;
    
        mesh1.rotation.x += delta;
        mesh1.rotation.y += delta;
    
        mesh2.rotation.x += delta;
        mesh2.rotation.y += delta;
    
        var elapsed = clock.getElapsedTime();
        cube.position.x = Math.cos(elapsed);
        cube.position.y = Math.sin(elapsed);
        renderer.render(scene, camera);
    }
    
    
    // renderer.render(scene, camera);
    render();
    */
}
var defautl = new Default();