var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(
    75, //fov
    window.innerWidth/window.innerHeight, //resolution
    0.1,
    1000);

var controls = new THREE.OrbitControls(camera);
controls.noKeys = false;

var renderer = new THREE.WebGLRenderer({
    antialias: true,
    alpha: true
});

renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

var geometryCube = new THREE.BoxGeometry(1,1,1);
var geometrySphere = new THREE.SphereGeometry(1,100,100);

var materialNormal = new THREE.MeshNormalMaterial();
var materialPhong = new THREE.MeshPhongMaterial({
    color: 0x00ee9a,
    shininess: 10000
    });
var materialLambert = new THREE.MeshLambertMaterial(0xaaaaaa);


var cube = new THREE.Mesh(geometryCube,materialNormal);
scene.add(cube);

var mesh1 = new THREE.Mesh(geometrySphere, materialPhong);
mesh1.position.x = -2;
scene.add(mesh1);

var mesh2 = new THREE.Mesh(geometrySphere.clone(), materialLambert);
mesh2.position.x = 2;
scene.add(mesh2)

var light = new THREE.DirectionalLight(0xdddddd,1);
light.position.set(0,0,1);
scene.add(light);



camera.position.x = 1;
camera.position.y = 1;
camera.position.z = 5;

var clock = new THREE.Clock();
var render = function(){
    requestAnimationFrame(render);
    var delta = clock.getDelta();
    cube.rotation.x += delta;
    cube.rotation.y += delta;
    
    mesh1.rotation.x += delta;
    mesh1.rotation.y += delta;

    mesh2.rotation.x += delta;
    mesh2.rotation.y += delta;

    var elapsed = clock.getElapsedTime();
    cube.position.x = Math.cos(elapsed);
    cube.position.y = Math.sin(elapsed);
    renderer.render(scene, camera);
}


// renderer.render(scene, camera);
render();