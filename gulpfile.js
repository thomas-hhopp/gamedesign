var gulp = require('gulp');
var watch = require('gulp-watch');
var ts = require('gulp-typescript');

var tsFolder = "./ts/**/*.ts";
var buildFolder = "./js"


gulp.task('default', ["buildts", "watch"]);



gulp.task("buildts", function () {

  return gulp.src(tsFolder)
    .pipe(ts({
      outFile: 'scene.js'
    }))
    .pipe(gulp.dest(buildFolder));

})

gulp.task("watch", function () {

  gulp.watch(tsFolder, ['buildts']);



})