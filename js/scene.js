/// <reference path="../typings/index.d.ts" />
var Default = (function () {
    function Default() {
        this.scene = new THREE.Scene();
        this.camera = this.createCamera();
        this.controls = this.createControls();
        this.renderer = this.createRender();
        this.setupRenderer();
        this.createCube();
        this.renderer.render(this.scene, this.camera);
    }
    Default.prototype.createCamera = function () {
        var camera = new THREE.PerspectiveCamera(75, //fov
        window.innerWidth / window.innerHeight, //resolution
        0.1, 1000);
        camera.position.x = 1;
        camera.position.y = 1;
        camera.position.z = 5;
        return camera;
    };
    Default.prototype.createControls = function () {
        var controls = new THREE.OrbitControls(this.camera);
        controls.noKeys = false;
        return controls;
    };
    Default.prototype.createRender = function () {
        return new THREE.WebGLRenderer({
            antialias: true,
            alpha: true
        });
    };
    Default.prototype.setupRenderer = function () {
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        document.body.appendChild(this.renderer.domElement);
    };
    Default.prototype.createCube = function () {
        var geometryCube = new THREE.BoxGeometry(1, 1, 1);
        var materialNormal = new THREE.MeshNormalMaterial();
        var cube = new THREE.Mesh(geometryCube);
        this.scene.add(cube);
    };
    return Default;
}());
var defautl = new Default();
